package com.bawono.monitoring.promotheusgrafana;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PromotheusgrafanaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PromotheusgrafanaApplication.class, args);
	}

}
